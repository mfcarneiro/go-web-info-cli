package app

import (
	"fmt"
	"log"
	"net"

	"github.com/urfave/cli"
)

func fetchIPs(c *cli.Context) {

	host := c.String("host")

	ips, error := net.LookupIP(host)

	if error != nil {
		log.Fatal(error)
	}

	for _, ip := range ips {
		fmt.Println(ip)
	}
}

func fetchServerNames(c *cli.Context) {
	serverName := c.String("host")

	servers, error := net.LookupNS(serverName)

	if error != nil {
		log.Fatal(error)
	}

	for _, server := range servers {
		fmt.Println(server.Host)
	}

}

func CreateCLI() *cli.App {
	app := cli.NewApp()
	app.HelpName = "CLI App based"
	app.Usage = "IP lookup CLI and Server name"

	flags := []cli.Flag{
		cli.StringFlag{
			Name:  "host",
			Value: "google.com",
		},
	}

	app.Commands = []cli.Command{
		{
			Name:  "ip",
			Usage: "Search for IP addresses",
			Flags: flags, Action: fetchIPs,
		},
		{
			Name:   "servers",
			Usage:  "Fetch the name of servers",
			Flags:  flags,
			Action: fetchServerNames,
		},
	}

	return app
}
