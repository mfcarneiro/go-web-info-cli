package main

import (
	"go-web-info-cli/app"
	"log"
	"os"
)

func main() {
	application := app.CreateCLI()

	if error := application.Run(os.Args); error != nil {
		log.Fatal(error)
	}
}
